import circle1 from "../assets/background/koła_01.png";
import circle2 from "../assets/background/koła_02.png";
import circle3 from "../assets/background/koła_03.png";

const Images = () => {
  return (
    <>
      <img src={circle3} className="circle3" alt="pic" />
      <img src={circle1} className="circle1" alt="pic" />
      <img src={circle2} className="circle2" alt="pic" />
    </>
  );
};

export default Images;
