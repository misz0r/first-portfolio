import menu from "../assets/background/_elementy_gradient_menu.png";

const Skills = ({ refSkills }) => {
  return (
    <div className="skills" ref={refSkills}>
      <h3>Skills</h3>
      <p>
        Duis aute irure dolor in reprehenderit in voluptate velit <br />
        esse cillum dolore eu fugiat nulla pariatur.
      </p>
      <div className="bar">
        <div className="PHP bars">
          <p>PHP 100%</p>
          <img src={menu} alt="pic" />
        </div>
        <div className="JS bars">
          <p>JS 90%</p>
          <img src={menu} alt="pic" />
        </div>
        <div className="HTML bars">
          <p>HTML 90%</p>
          <img src={menu} alt="pic" />
        </div>
        <div className="NODEJS bars">
          <p>NODEJS 60%</p>
          <img src={menu} alt="pic" />
        </div>
        <div className="CSS bars">
          <p>CSS 90%</p>
          <img src={menu} alt="pic" />
        </div>
        <div className="GO bars">
          <p>GO 60%</p>
          <img src={menu} alt="pic" />
        </div>
      </div>
    </div>
  );
};

export default Skills;
