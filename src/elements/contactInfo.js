import contactIcon from "../assets/png/contact_ikona.png";
import me2 from "../assets/img/moje-zdj-2.jpg";

const ContactInfo = () => {

  return (
    <>
      <div className="contact-window">
        <img src={contactIcon} id="icon" alt="pic" />
        <div>
          <p>maciejmyszka99@gmail.com</p>
          <p>+48 884 336 133</p>
        </div>
      </div>
      <div className="contact-right">
        <div className="contact-info">
          <img src={me2} alt="pic" />
          <p>
            author: Maciej Myszka
            <br />
            username: @misz0r
            <br />
            description: Uniwersytet Ekonomiczny
            <br />
            homepage: maciejmyszka.github.com
            <br />
            repository type: Open-source
            <br />
            url: github.com/maciejmyszka
          </p>
        </div>
      </div>
    </>
  );
};

export default ContactInfo;
