import circle from "../assets/background/koła_01.png";

const ContentTools = () => {
  return (
    <>
      <img src={circle} className="circle11" alt="pic" />
      <h3>
        Tools <br />
        <span>My essentials</span>
      </h3>
    </>
  );
};

export default ContentTools;
