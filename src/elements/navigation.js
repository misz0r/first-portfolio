import twitter from "../assets/png/twitter_icon.png";
import facebook from "../assets/png/facebook_icon.png";
import linkedin from "../assets/png/linkedin_icon.png";
import { useState } from "react";
import "./navigation.css";

const Navigation = ({
  refAbout,
  refSkills,
  refPortfolio,
  refBlog,
  refContact,
}) => {
  const [hideNav, setHideNav] = useState(true);

  const scrollToRef = (ref) => ref.current.scrollIntoView();

  return (
    <>
      <div className="main-nav">
        <div className="menu">
          <img
            src="https://s2.svgbox.net/hero-outline.svg?ic=menu&color=dc8228"
            width="32"
            height="32"
            alt=""
            className="hamburger"
            onClick={() => setHideNav((prevState) => !prevState)}
          />
        </div>
        <ul className={hideNav ? "hidden" : null}>
          <li onClick={() => scrollToRef(refAbout)}>About me</li>
          <li onClick={() => scrollToRef(refSkills)}>Skills</li>
          <li onClick={() => scrollToRef(refPortfolio)}>Portfolio</li>
          <li onClick={() => scrollToRef(refBlog)}>Blog</li>
          <li onClick={() => scrollToRef(refContact)}>Contact me</li>
        </ul>

        <div className="main-nav__icons">
          <img src={twitter} alt="icon" />
          <img src={facebook} alt="icon" />
          <img src={linkedin} alt="icon" />
        </div>
      </div>
    </>
  );
};

export default Navigation;
