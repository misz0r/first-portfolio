import easyButton from "../assets/png/easy_code_button.png";
const AboutMe = () => {
  return (
    <div className="aboutme">
      <div className="aboutme__text">
        <h3>
          About me
          <br />
          <span>All about Techy</span>
        </h3>
        <br />
        <p>Lorem impsum dolor sit amet,</p>
        <p>
          consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
          labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
          exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        </p>
        <p>
          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
          dolore eu fugiat nulla pariatur.
        </p>
        <h3>
          <span>My interests</span>
        </h3>
        <ul>
          <li>music</li>
          <li>kitesurfing</li>
          <li>cycling</li>
        </ul>

        <div className="bottom">
          <h4>Ukończyłem kurs Easy Code</h4>
          <img src={easyButton} alt="pic" />
        </div>
      </div>
    </div>
  );
};

export default AboutMe;
