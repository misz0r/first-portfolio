import bitbucket from "../assets/png/bit_bucket_icon.png";
import dev from "../assets/png/dev_icon.png";

const ContentWindow = () => {
  return (
    <div className="content-window">
      <h3>
        Hi, My name is Maciej Myszka
        <br />
        <span>Sortware Engineer</span>
      </h3>
      <p>
        Passionate Techy and Tech Author <br />
        with 3 years of experience within the field.
      </p>
      <div className="window__description">
        <p>See my works</p>
        <img src={bitbucket} alt="" />
        <img src={dev} alt="" />
      </div>
    </div>
  );
};

export default ContentWindow;
