import reactIcon from "../assets/png/_react_icon_s.png";
import webpack from "../assets/png/webpack_icon.png";
import express from "../assets/png/express_icon.png";
import styled from "../assets/png/styled_components_icon.png";
import flexbox from "../assets/png/flexbox_icon_s.png";
import program from "../assets/png/tool_program_ikona.png";

const ToolsIcons = () => {
  return (
    <div className="tools-icons">
      <div className="icons">
        <img src={reactIcon} alt="pic" />
        <figcaption>
          React
          <br />
          16.6.3
        </figcaption>
      </div>

      <div className="icons">
        <img src={webpack} alt="pic" />
        <figcaption>
          Webpack
          <br />
          4.191
        </figcaption>
      </div>

      <div className="icons">
        <img src={express} alt="pic" />
        <figcaption>
          Express
          <br />
          4.16.4
        </figcaption>
      </div>

      <div className="icons">
        <img src={styled} alt="pic" />
        <figcaption>
          Styled
          <br />
          Components
          <br />
          4.16.4
        </figcaption>
      </div>

      <div className="icons">
        <img src={flexbox} alt="pic" />
        <figcaption>
          Flexbox
          <br />
          4.01
        </figcaption>
      </div>

      <div className="icon-program">
        <img src={program} alt="pic" />
        <figcaption>
          Program
          <br />
          5.21
        </figcaption>
      </div>

      <div className="icon-program">
        <img src={program} alt="pic" />
        <figcaption>
          Program
          <br />
          5.22
        </figcaption>
      </div>
    </div>
  );
};

export default ToolsIcons;
