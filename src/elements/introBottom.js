const IntroBottom = () => {
  return (
    <div className="intro-bottom">
      <h3>I am a freelancer</h3>
      <p>Contact me if you want to work with me</p>
      <form>
        <input type="button" value="Hire me" />
        <input type="button" value="Download CV" />
      </form>
    </div>
  );
};

export default IntroBottom;
