import blog1 from "../assets/img/blog_post-1.jpg";
import blog2 from "../assets/img/blog_post-2.jpg";

const BlogPosts = ({ refBlog }) => {
  return (
    <div className="main-content__blogposts" ref={refBlog}>
      <h3>
        Blog posts
        <br />
        <span>Hints and tips</span>
      </h3>
      <div className="blogpost">
        <img src={blog1} alt="pic" />
        <div className="blog__description">
          <div className="description__top">
            <h3>
              Title 01
              <br />
              <span>Secondary Title</span>
            </h3>
            <p>author, 01.09.2020</p>
          </div>
          <p>
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
            nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur.
          </p>
        </div>
      </div>

      <div className="blogpost">
        <img src={blog2} alt="pic" />
        <div className="blog__description">
          <div className="description__top">
            <h3>
              Title 02
              <br />
              <span>Secondary Title</span>
            </h3>
            <p>author, 01.09.2020</p>
          </div>
          <p>
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
            nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur.
          </p>
        </div>
      </div>
    </div>
  );
};

export default BlogPosts;
