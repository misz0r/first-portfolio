const ContactForm = () => {
  return (
    <div className="contact-form">
      <h3>Contact me</h3>
      <p>
        If you are willing to work with me, contact me. I can join <br /> your
        conference to serve you with my engeneering experience.
      </p>
      <form>
        <input type="email" name="email" placeholder="Your e-mail" /> <br />
        <input type="name" name="name" placeholder="Your name" /> <br />
        <textarea
          placeholder="How can I help you? 
                                           Please, put here your
                                           message/request."
        ></textarea>
        <input type="submit" id="submit" name="" value="Wyślij" />
      </form>
    </div>
  );
};

export default ContactForm;
