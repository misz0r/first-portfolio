import case1 from "../assets/png/portfolio_case_01.png";
import case2 from "../assets/png/portfolio_case_02.png";
import case3 from "../assets/png/portfolio_case_03.png";
import case4 from "../assets/png/portfolio_case_04.png";
import case5 from "../assets/png/portfolio_case_05.png";
import case6 from "../assets/png/portfolio_case_06.png";
import bitbucket from "../assets/png/bit_bucket_icon.png";
import extIcon from "../assets/png/external_link_icon.png";

const Projects = () => {
  return (
    <div className="portfolio-projects">
      <div className="projects">
        <img src={case1} className="case" alt="pic" />
        <div className="hover">
          <h3>Office software</h3>
          <ul>
            <li>task management</li>
          </ul>
          <p>
            Tools: React / Redux /<br />
            Program 01 / Flexbox
          </p>
        </div>
        <div className="small">
          <img src={bitbucket} alt="pic" />
          <img src={extIcon} alt="pic" />
        </div>
      </div>

      <div className="projects">
        <img src={case2} className="case" alt="pic" />
        <div className="hover">
          <h3>Office software</h3>
          <ul>
            <li>task management</li>
          </ul>
          <p>
            Tools: React / Redux /<br />
            Program 01 / Flexbox
          </p>
        </div>
        <div className="small">
          <img src={bitbucket} alt="pic" />
          <img src={extIcon} alt="pic" />
        </div>
      </div>

      <div className="projects">
        <img src={case3} className="case" alt="pic" />
        <div className="hover">
          <h3>Office software</h3>
          <ul>
            <li>task management</li>
          </ul>
          <p>
            Tools: React / Redux /<br />
            Program 01 / Flexbox
          </p>
        </div>
        <div className="small">
          <img src={bitbucket} alt="pic" />
          <img src={extIcon} alt="pic" />
        </div>
      </div>

      <div className="projects">
        <img src={case4} className="case" alt="pic" />
        <div className="hover">
          <h3>Office software</h3>
          <ul>
            <li>task management</li>
          </ul>
          <p>
            Tools: React / Redux /<br />
            Program 01 / Flexbox
          </p>
        </div>
        <div className="small">
          <img src={bitbucket} alt="pic" />
          <img src={extIcon} alt="pic" />
        </div>
      </div>

      <div className="projects">
        <img src={case5} className="case" alt="pic" />
        <div className="hover">
          <h3>Office software</h3>
          <ul>
            <li>task management</li>
          </ul>
          <p>
            Tools: React / Redux /<br />
            Program 01 / Flexbox
          </p>
        </div>
        <div className="small">
          <img src={bitbucket} alt="pic" />
          <img src={extIcon} alt="pic" />
        </div>
      </div>

      <div className="projects">
        <img src={case6} className="case" alt="pic" />
        <div className="hover">
          <h3>Office software</h3>
          <ul>
            <li>task management</li>
          </ul>
          <p>
            Tools: React / Redux /<br />
            Program 01 / Flexbox
          </p>
        </div>
        <div className="small">
          <img src={bitbucket} alt="pic" />
          <img src={extIcon} alt="pic" />
        </div>
      </div>
    </div>
  );
};

export default Projects;
