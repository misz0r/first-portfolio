import circle1 from "../assets/background/koła_01.png";

const MyWork = () => {
  return (
    <>
      <img src={circle1} className="circle12" alt="pic" />
      <h3>
        My works
        <br />
        <span>Portfolio</span>
      </h3>
      <p>
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
        <br />
        nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
        <br />
        reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
        <br />
        pariatur.
      </p>
    </>
  );
};

export default MyWork;
