import Signature from "../elements/signature";
import Navigation from "../elements/navigation";

const Header = ({ refAbout, refSkills, refPortfolio, refBlog, refContact }) => {
  return (
    <div className="main-header">
      <Signature />
      <Navigation
        {...{ refAbout, refSkills, refPortfolio, refBlog, refContact }}
      />
    </div>
  );
};

export default Header;
