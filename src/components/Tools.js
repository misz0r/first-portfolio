import ContentTools from "../elements/contentTools";
import ToolsIcons from "../elements/toolsIcons";

const Tools = () => {
  return (
    <div className="main-content__tools">
      <ContentTools />
      <ToolsIcons />
    </div>
  );
};

export default Tools;
