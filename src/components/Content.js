import me from "../assets/img/moje-zdj-1.jpg";
import ContentWindow from "../elements/contentWindow";
import IntroBottom from "../elements/introBottom";
import AboutMe from "../elements/aboutMe";
import Skills from "../elements/skills";
import Images from "../elements/images";

const Content = ({ refAbout, refSkills }) => {
  return (
    <div className="main-content__wrapper" ref={refAbout}>
      <Images />
      <div className="main-content__intro">
        <img src={me} id="zdj" alt="" />
        <ContentWindow />
        <IntroBottom />
      </div>
      <div className="main-content__knowledge">
        <AboutMe />
        <Skills {...{ refSkills }} />
      </div>
    </div>
  );
};

export default Content;
