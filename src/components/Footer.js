import ContactForm from "../elements/contactForm";
import ContactInfo from "../elements/contactInfo";
import circle2 from "../assets/background/koła_02.png";

const Footer = ({ refContact }) => {
  return (
    <div className="main-footer__contact" ref={refContact}>
      <img src={circle2} className="circle13" alt="pic" />
      <ContactForm />
      <ContactInfo />
    </div>
  );
};

export default Footer;
