import BlogPosts from "../elements/blogPosts";
import MyWork from "../elements/myWork";
import Projects from "../elements/projects";

const Portfolio = ({ refPortfolio, refBlog }) => {
  return (
    <div className="main-content__wrapper" ref={refPortfolio}>
      <div className="main-content__portfolio">
        <MyWork />
        <Projects />
      </div>
      <BlogPosts {...{ refBlog }} />
    </div>
  );
};

export default Portfolio;
