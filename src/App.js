import "./App.css";
import Header from "./components/Header";
import Content from "./components/Content";
import Tools from "./components/Tools";
import Portfolio from "./components/Portfolio";
import Footer from "./components/Footer";
import { useRef } from "react";
import "./styles/themes/default/theme.scss";

function App() {
  const refSkills = useRef(null);
  const refAbout = useRef(null);
  const refPortfolio = useRef(null);
  const refContact = useRef(null);
  const refBlog = useRef(null);

  return (
    <div className="App">
      <Header {...{ refAbout, refSkills, refPortfolio, refBlog, refContact }} />
      <Content {...{ refAbout, refSkills }} />
      <Tools />
      <Portfolio {...{ refPortfolio, refBlog }} />
      <Footer {...{ refContact }} />
    </div>
  );
}

export default App;
